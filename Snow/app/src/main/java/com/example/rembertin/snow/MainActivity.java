package com.example.rembertin.snow;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Exception;
import java.lang.Override;
import java.net.URI;
import java.net.URL;

public class MainActivity extends ActionBarActivity {
    public TextView etatStationValue;
    public TextView meteoActuelleValue;
    public TextView temperatureAMValue;
    public TextView temperaturePMValue;
    public TextView vitesseVentValue;
    public TextView enneigementValue;

    private EditText saisieStation;
    private CheckBox vibrationSwitch;

    public Context context;

    public final static int REQUEST_CODE = 0;
    private final static String PREFS_FILE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etatStationValue = (TextView) findViewById(R.id.etatStationValue);
        meteoActuelleValue = (TextView) findViewById(R.id.meteoActuelleValue);
        temperatureAMValue = (TextView) findViewById(R.id.temperatureAMValue);
        temperaturePMValue = (TextView) findViewById(R.id.temperaturePMValue);
        vitesseVentValue = (TextView) findViewById(R.id.vitesseVentValue);
        enneigementValue = (TextView) findViewById(R.id.enneigementValue);
        saisieStation = (EditText) findViewById(R.id.saisieStation);
        vibrationSwitch = (CheckBox) findViewById(R.id.vibrationSwitch);
        restoreParameters();
    }

    public void okClick(View view){
        callWeatherService();
    }

    private void callWeatherService() {
        GetWeatherTask request = new GetWeatherTask(context);
        request.execute("http://snowlabri.appspot.com/snow?station=" + Uri.encode(saisieStation.getText().toString()));
        saveParameters();
    }

    public void locationClick(View view){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("geo:0,0?q=" + saisieStation.getText()));
        startActivity(intent);
        saveParameters();
    }

    public void listStationsClick(View view) {
        startActivityForResult(new Intent(this, StationListActivity.class), REQUEST_CODE);
    }

    public void saveParameters() {
        SharedPreferences settings = getSharedPreferences(PREFS_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("saisieStation", saisieStation.getText().toString());
        editor.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            saisieStation.setText(data.getStringExtra("station"));
            callWeatherService();
        }
    }

    public void restoreParameters(){
        SharedPreferences settings = getSharedPreferences(PREFS_FILE,
                Activity.MODE_PRIVATE);
        saisieStation.setText(settings.getString("saisieStation", null));
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("etatStation", etatStationValue.getText().toString());
        savedInstanceState.putString("meteoActuelle", meteoActuelleValue.getText().toString());
        savedInstanceState.putString("temperatureAM", temperatureAMValue.getText().toString());
        savedInstanceState.putString("temperaturePM", temperaturePMValue.getText().toString());
        savedInstanceState.putString("vitesseVent", vitesseVentValue.getText().toString());
        savedInstanceState.putString("enneigement", enneigementValue.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etatStationValue.setText(savedInstanceState.getString("etatStation"));
        meteoActuelleValue.setText(savedInstanceState.getString("meteoActuelle"));
        temperatureAMValue.setText(savedInstanceState.getString("temperatureAM"));
        temperaturePMValue.setText(savedInstanceState.getString("temperaturePM"));
        vitesseVentValue.setText(savedInstanceState.getString("vitesseVent"));
        enneigementValue.setText(savedInstanceState.getString("enneigement"));
    }

    public void updateWidgets(String result){
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString("ouverte").equals("oui"))
                etatStationValue.setText("Ouverte");
            else
                etatStationValue.setText("Fermée");
            String meteoText = jsonObject.getString("temps");
            meteoText = meteoText.substring(0,1).toUpperCase() + meteoText.substring(1);
            meteoActuelleValue.setText(meteoText);
            temperatureAMValue.setText(jsonObject.getString("temperatureMatin").replace("C", "°C"));
            temperaturePMValue.setText(jsonObject.getString("temperatureMidi").replace("C", "°C"));
            vitesseVentValue.setText(jsonObject.getString("vent"));
            enneigementValue.setText(jsonObject.getString("neige"));
        }
        catch (JSONException e)
        {
            Log.i("Exception Json", e.getMessage());
        }
        catch (Exception e)
        {
            Log.i("Autre exception", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Interroge le service web du LabRi
     */
    public class GetWeatherTask extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        public GetWeatherTask(Context context) {
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setTitle("Interrogation du service Web...");
            progressDialog.setMessage("Veuillez patienter...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... uri) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {
                response = httpclient.execute(new HttpGet(uri[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } else {
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return responseString;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            updateWidgets(result);
            if (progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            if (vibrationSwitch.isChecked())
            {
                Vibrator vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(250);
            }
        }
    }
}