package com.example.rembertin.snow;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;


public class StationListActivity extends Activity {

    private ListView listStations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_list);
        listStations = (ListView)findViewById(R.id.listView);
        fillList();
        listStations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView selected = (TextView)view;
                Intent data = new Intent();
                data.putExtra("station", selected.getText());
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, data);
                }
                else {
                    getParent().setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
    }

    private void fillList() {
        ArrayList<String> stations = new ArrayList<>(Arrays.asList("Gourette", "Font Romeu", "Saint Lary", "Les Angles", "Peyragudes", "La Mongie", "Cauterets", "Arette", "Luz Ardiden"));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.text_view_layout, stations);
        listStations.setAdapter(arrayAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_station_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
